// console.log("balsa wood bones");

// Exponent Operator
let getCube = x => x ** 3;


// Template Literals
let y = 7;
console.log(`The cube of ${y} is ${getCube(y)}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [streetNumber, street, city, zipCode] = address;
console.log(`My shipping address is ${streetNumber} ${street}, ${city}, ${zipCode}`);


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 killograms",
	measurement: "20 feet 3 inches"
}

let {name, species, weight, measurement} = animal;
console.log(`We saw a ${species} named ${name} at the zoo and he was so big! About ${measurement} and keeper said he was a ${weight}!`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach(x => console.log(x));


// Javascript Classes
class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}

let freelancePolice = new Dog("Sam", 47, "Irish Wolfhound");
let teamFriendship = new Dog("Perrito", "unknown", "Mixed");

console.log(freelancePolice);
console.log(teamFriendship);